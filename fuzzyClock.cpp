#include <time.h>
#include <QSettings>
#include "fuzzyClock.h"
#include "fuzzyClockWindow.h"

int fuzzyClock::getHourNow()
{
    time_t t = time(0);   // get time now
    tm* now = localtime(&t);
    return now->tm_hour;    
}

int fuzzyClock::getMinuteNow()
{
    time_t t = time(0);   // get time now
    tm* now = localtime(&t);
    return now->tm_min;
}

void fuzzyClock::SetLabel(QLabel *timeLabel){
    m_label = timeLabel;
    m_label->setObjectName("timeLabel"); // for CSS
//    QFont timeFont("Tahoma", 10, QFont::Bold);
//    m_label->setFont(timeFont);
}

void fuzzyClock::DisplayTime(){

    hourToUse = getHourNow();
    // если время в диапазоне от 12 до 23, чтобы
    // из массива считывались значения от 0 до 11
    if (hourToUse > 11){
        hourToUse = getHourNow() - 12;
    }

    referMinPos = posRefer[getMinuteNow()];
    timeToShow = fuzzyMinutes[referMinPos];

    if (referMinPos >= 0 && referMinPos <= 2)
        if (hourToUse == 12) hourToUse = 11;
         else hourToUse--; // "Ровно ХХ"

//    QString nomHour = nominativeHour[hourToUse];
//    QString genHour = genitiveHour[hourToUse];

//    if (12 == getHourNow() && 0 == getMinuteNow())
//        timeToShow = "Полдень";

//    if (0 == getHourNow() && 0 == getMinuteNow())
//        timeToShow = "Полночь";

    if (timeToShow.contains("%0")){
        timeToShow.replace("%0", "%1");
        timeToShow = QString(fuzzyMinutes[referMinPos]).arg(nominativeHour[hourToUse]);
    }
    if (timeToShow.contains("%1")){
        //timeToShow.replace("%0", "%1");
        timeToShow = QString(fuzzyMinutes[referMinPos]).arg(genitiveHour[hourToUse]);
    }

    m_label->setText(timeToShow);
    fWidth = m_label->fontMetrics().boundingRect(m_label->text()).width();
    fHeight = m_label->fontMetrics().boundingRect(m_label->text()).height();
    m_label->setGeometry(5, 5, fWidth, fHeight);
    m_label->setFixedWidth(fWidth);
    m_window->setFixedSize(fWidth + 10, fHeight + 10);
}

void fuzzyClock::SetWindow(QWidget *pWindow){
    m_window = pWindow;
}

fuzzyClock::fuzzyClock(WId pWindow){}
fuzzyClock::~fuzzyClock(){}
